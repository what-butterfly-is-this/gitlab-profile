# What Butterfly Is This

Final project for the course Advanced Computational Techniques for Big Imaging and Signal Data.

Artificial Intelligence for Science and Technology, University of Milano-Bicocca, A.Y. 2022/23.

Developed by Alberto Mosconi [student id: 898685]

## Description

The project consists in a mobile app to classify the species of butterflies and moths with Artificial Intelligence.

## Project Structure

The code for this project is split into 3 repositories:

- [**app**](https://gitlab.com/what-butterfly-is-this/app): the client application that allows users to photograph a butterfly or moth and identify its species by sending the image to a server where a Machine Learning model classifies it.  It is built with Expo, a framework for building cross-platform React Native apps.

- [**server**](https://gitlab.com/what-butterfly-is-this/server): the server API, written in python with Flask. Accepts images via POST request and performs some preprocessing steps before giving them as input to a classification model. Returns the predicted class along with some additional information and wikipedia link. The code is deployed on AWS EC2.

- [**model**](https://gitlab.com/what-butterfly-is-this/model): the code for the main model responsible for classifying the images. Written in PyTorch, comprises dataset exploration and pre-processing, different network architectures and performance evaluation.

## Download App

**Only for Android**

The app is available for download in the [app's `releases` tab](https://gitlab.com/what-butterfly-is-this/app/-/releases/), or simply by clicking [this link](https://gitlab.com/what-butterfly-is-this/app/uploads/cc4af98882a4d2294bb0ed4fc25fa16a/whatbutterflyisthis.apk).
